#!/bin/bash

# Get the DOCKER variable for running with/without sudo
source scripts_aux/superuser.sh

CMD="$DOCKER build --build-arg uid=$(id -u) --build-arg gid=$(id -g) --no-cache -t chia-hpool:latest ./docker/"
eval $CMD
