#!/bin/bash

# Get the DOCKER variable for running with/without sudo
source scripts_aux/superuser.sh

CURRENT_DIR=$(dirname "$(readlink -f "$0")")

mkdir -p $CURRENT_DIR/x-proxy/log

CMD="$DOCKER run --rm -it \
        -p 9190:9190 \
        --mount type=bind,src=$CURRENT_DIR/x-proxy/config.yaml,dst=/home/farmer/x-proxy/config.yaml \
        --mount type=bind,src=$CURRENT_DIR/x-proxy/log,dst=/home/farmer/x-proxy/log \
        -w /home/farmer/x-proxy \
        chia-hpool"
eval $CMD
