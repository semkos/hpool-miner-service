FROM ubuntu:20.04

ARG uid
ARG gid

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y unzip

RUN apt-get install -y locales
RUN dpkg-reconfigure locales
RUN locale-gen en_US.UTF-8
RUN update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
ENV LANG=en_US.UTF-8

RUN rm -rf /var/lib/apt/lists/*

RUN groupadd -g $gid farmer
RUN useradd -u $uid -g $gid -m -s /bin/bash farmer

ARG hpool_plotter_tag=v0.11
ARG hpool_plotter_file=chia-plotter-v0.11-x86_64-linux-gnu.zip
ARG hpool_miner_tag=v1.4.1-1
ARG hpool_miner_file=HPool-Miner-chia-v1.4.1-0-linux.zip
ARG x_proxy_file=x-proxy.v1.0.0.zip

ADD https://github.com/hpool-dev/chia-plotter/releases/download/$hpool_plotter_tag/$hpool_plotter_file /tmp
ADD https://github.com/hpool-dev/chia-miner/releases/download/$hpool_miner_tag/$hpool_miner_file /tmp
ADD https://github.com/hpool-dev/chia-miner/releases/download/$hpool_miner_tag/$x_proxy_file /tmp

RUN chown farmer:farmer /tmp/$hpool_plotter_file
RUN chown farmer:farmer /tmp/$hpool_miner_file
RUN chown farmer:farmer /tmp/$x_proxy_file

USER farmer

RUN unzip /tmp/$hpool_plotter_file -d /home/farmer/hpool-plotter
RUN unzip /tmp/$hpool_miner_file -d /home/farmer/hpool-miner
RUN unzip /tmp/$x_proxy_file -d /home/farmer/x-proxy
RUN rm /tmp/$hpool_plotter_file /tmp/$hpool_miner_file
