#!/bin/bash

# Get the DOCKER variable for running with/without sudo
source scripts_aux/superuser.sh

source dir-mounts.sh

CMD="$DOCKER run --rm -it \
        $DIR_MOUNTS \
        -w /home/farmer/hpool-miner/linux \
        chia-hpool ./hpool-miner-chia"
eval $CMD
