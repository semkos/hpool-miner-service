if getent group docker | grep &>/dev/null "\b${USER}\b"; then
    DOCKER="docker"
else
    echo "[!] Using Root permissions to run docker"
    DOCKER="sudo docker"
fi

