#!/bin/bash

CURRENT_DIR=$(dirname "$(readlink -f "$0")")

mkdir -p $CURRENT_DIR/hpool/log

DIR_MOUNTS=" \
    --mount type=bind,src=$CURRENT_DIR/hpool/config.yaml,dst=/home/farmer/hpool-miner/linux/config.yaml \
    --mount type=bind,src=$CURRENT_DIR/hpool/log,dst=/home/farmer/hpool-miner/linux/log \
    "